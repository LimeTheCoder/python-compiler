class ConsoleView:
    def __init__(self, message):
        self.__message = message

    def display(self, model_info):
        lexemes, _, _ = model_info

        if not lexemes:
            return

        print self.__message
        for lexeme in lexemes:
            print lexeme.position.row, '\t', lexeme.position.column, '\t', lexeme.code, '\t', lexeme.value

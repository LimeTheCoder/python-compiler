from model.lexeme import Position


class Scanner:
    def __init__(self, filename):
        self.__file = open(filename, 'r')
        self.__currPos = Position(1, 1)
        self.__checkpoint = Position(1, 1)

    def read_next(self):
        ch = self.__file.read(1)

        if ch == '\n':
            self.__currPos.row += 1
            self.__currPos.column = 1
        else:
            self.__currPos.column += 1

        return ch

    def make_checkpoint(self):
        self.__checkpoint = Position(self.__currPos.row, self.__currPos.column)

    def get_current_position(self):
        return Position(self.__currPos.row, self.__currPos.column)

    def get_last_checkpoint(self):
        return Position(self.__checkpoint.row, self.__checkpoint.column)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__file.close()
